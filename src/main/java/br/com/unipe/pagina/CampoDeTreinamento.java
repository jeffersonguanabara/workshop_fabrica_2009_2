package br.com.unipe.pagina;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CampoDeTreinamento {

	@Ignore
	@Test
	public void cadastroSimples() throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "/home/guanabara/Downloads/chromedriver");
		
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
		
		WebElement nome = driver.findElement(By.id("elementosForm:nome"));
		WebElement sobrenome = driver.findElement(By.id("elementosForm:sobrenome"));
		WebElement sexo = driver.findElement(By.id("elementosForm:sexo:0"));
		WebElement comida1 = driver.findElement(By.id("elementosForm:comidaFavorita:0"));
		WebElement comida2 = driver.findElement(By.id("elementosForm:comidaFavorita:1"));
		WebElement comida3 = driver.findElement(By.id("elementosForm:comidaFavorita:2"));
		WebElement escolaridade = driver.findElement(By.id("elementosForm:escolaridade"));
		WebElement esporte = driver.findElement(By.id("elementosForm:esportes"));
		WebElement sugestoes = driver.findElement(By.id("elementosForm:sugestoes"));
		
		WebElement inputButton01 = driver.findElement(By.xpath("//*[@id=\'elementosForm:tableUsuarios\']/tbody/tr[1]/td[3]/input"));
		WebElement checkBox01 = driver.findElement(By.xpath("//*[@id=\'elementosForm:tableUsuarios\']/tbody/tr[1]/td[4]/input"));
		WebElement radioButton01 = driver.findElement(By.xpath("//*[@id='elementosForm:tableUsuarios']/tbody/tr[1]/td[5]/table/tbody/tr/td/input"));
		WebElement inputText01 = driver.findElement(By.xpath("//*[@id=\'elementosForm:tableUsuarios\']/tbody/tr[1]/td[6]/input"));
		
		WebElement inputButton02 = driver.findElement(By.xpath("//*[@id=\'elementosForm:tableUsuarios\']/tbody/tr[2]/td[3]/input"));
		WebElement checkBox02 = driver.findElement(By.xpath("//*[@id=\'elementosForm:tableUsuarios\']/tbody/tr[2]/td[4]/input"));
		WebElement radioButton02 = driver.findElement(By.xpath("//*[@id='elementosForm:tableUsuarios']/tbody/tr[2]/td[5]/table/tbody/tr/td/input"));
		WebElement inputText02 = driver.findElement(By.xpath("//*[@id=\'elementosForm:tableUsuarios\']/tbody/tr[2]/td[6]/input"));
		
		WebElement inputButton03 = driver.findElement(By.xpath("//*[@id=\'elementosForm:tableUsuarios\']/tbody/tr[3]/td[3]/input"));
		WebElement checkBox03 = driver.findElement(By.xpath("//*[@id=\'elementosForm:tableUsuarios\']/tbody/tr[3]/td[4]/input"));
		WebElement radioButton03 = driver.findElement(By.xpath("//*[@id='elementosForm:tableUsuarios']/tbody/tr[3]/td[5]/table/tbody/tr/td/input"));
		WebElement inputText03 = driver.findElement(By.xpath("//*[@id=\'elementosForm:tableUsuarios\']/tbody/tr[3]/td[6]/input"));
		
		WebElement inputButton04 = driver.findElement(By.xpath("//*[@id=\'elementosForm:tableUsuarios\']/tbody/tr[4]/td[3]/input"));
		WebElement checkBox04 = driver.findElement(By.xpath("//*[@id=\'elementosForm:tableUsuarios\']/tbody/tr[4]/td[4]/input"));
		WebElement radioButton04 = driver.findElement(By.xpath("//*[@id='elementosForm:tableUsuarios']/tbody/tr[4]/td[5]/table/tbody/tr/td/input"));
		WebElement inputText04 = driver.findElement(By.xpath("//*[@id=\'elementosForm:tableUsuarios\']/tbody/tr[4]/td[6]/input"));
		
		WebElement inputButton05 = driver.findElement(By.xpath("//*[@id=\'elementosForm:tableUsuarios\']/tbody/tr[5]/td[3]/input"));
		WebElement checkBox05 = driver.findElement(By.xpath("//*[@id=\'elementosForm:tableUsuarios\']/tbody/tr[5]/td[4]/input"));
		WebElement radioButton05 = driver.findElement(By.xpath("//*[@id='elementosForm:tableUsuarios']/tbody/tr[5]/td[5]/table/tbody/tr/td/input"));
		WebElement inputText05 = driver.findElement(By.xpath("//*[@id=\'elementosForm:tableUsuarios\']/tbody/tr[5]/td[6]/input"));
		
		WebElement cadastar = driver.findElement(By.id("elementosForm:cadastrar"));
		
		Select selectEscolaridade = new Select(escolaridade);
		Select selectEsportes = new Select(esporte);
		List<WebElement> listaEsportes = selectEsportes.getOptions();	
		// Ações
		nome.clear();
		nome.sendKeys("Jefferson");
		
		sobrenome.clear();
		sobrenome.sendKeys("Guanabara");
		
		sexo.click();
		
		comida1.click();
		comida2.click();
		comida3.click();
		
		selectEscolaridade.selectByValue("superior");
		Assert.assertEquals("Superior", selectEscolaridade.getFirstSelectedOption().getText());
		
//		for (WebElement option : listaEsportes) {
//			System.out.println("oi");
//			if (option.getText().equals("Futebol")) {
//				System.out.println("entrou no if");
//				option.click();
//				System.out.println(option.getText());
//			}
//		}
		
		listaEsportes.forEach(l -> {
			if(l.getText().equals("Futebol"))
				l.click();
		});
		
		sugestoes.sendKeys("Nada a comentar!!!");
		
		inputButton01.click();
		Alert alertaOne = driver.switchTo().alert();
		String texto = alertaOne.getText();
		Assert.assertEquals("Francisco", texto);
		alertaOne.accept();
//		driver.switchTo().defaultContent();
//		System.out.println("AAAAAAAAA");
		checkBox01.click();
		radioButton01.click();
		inputText01.sendKeys("Paulo");
		
		inputButton02.click();
		Alert alertaTwo = driver.switchTo().alert();
		String texto02 = alertaTwo.getText();
		Assert.assertEquals("Maria", texto02);
		alertaTwo.accept();
		checkBox02.click();
		radioButton02.click();
		inputText02.sendKeys("Jesus");
		
		inputButton03.click();
		Alert alertaThree = driver.switchTo().alert();
		String texto03 = alertaThree.getText();
		Assert.assertEquals("Usuario A", texto03);
		alertaThree.accept();
		checkBox03.click();
		radioButton03.click();
		inputText03.sendKeys("Aaaaaa");
		
		inputButton04.click();
		Alert alertaFour = driver.switchTo().alert();
		String texto04 = alertaFour.getText();
		Assert.assertEquals("Doutorado", texto04);
		alertaFour.accept();
		checkBox04.click();
		radioButton04.click();
		inputText04.sendKeys("Bbbbb");
		
		inputButton05.click();
		Alert alertaFive = driver.switchTo().alert();
		String texto05 = alertaFive.getText();
		Assert.assertEquals("Usuario B", texto05);
		alertaFour.accept();
		checkBox05.click();
		radioButton05.click();
		inputText05.sendKeys("Teste");
		
		
		cadastar.click();
		System.out.println("Chegou aqui!");
		// Espera
		Thread.sleep(5000);
		
		// encerra o driver
		driver.quit();
		
	}
	
	@Test
	public void cenarioTeste02() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "/home/guanabara/Downloads/chromedriver");
		
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
		
		WebElement btnCadastrar = driver.findElement(By.id("elementosForm:cadastrar"));
		
		// Passo 1
		btnCadastrar.click();
		// Passo 2
		Alert alertNome = driver.switchTo().alert();
		String mensagemAlertNome = alertNome.getText();
		Assert.assertEquals("Nome eh obrigatorio", mensagemAlertNome);
		alertNome.accept();
//		driver.switchTo().window(" ");
		WebElement inputNome = driver.findElement(By.id("elementosForm:nome"));
		inputNome.sendKeys("Thiago");
		
		// Passo 3
		btnCadastrar.click();
		// Passo 4
		Alert alertSobrenome = driver.switchTo().alert();
		String mensagemAlertSobrenome = alertSobrenome.getText();
		Assert.assertEquals("Sobrenome eh obrigatorio", mensagemAlertSobrenome);
		alertSobrenome.accept();
//		driver.switchTo().window(" ");
		WebElement inputSobrenome = driver.findElement(By.id("elementosForm:sobrenome"));
		inputSobrenome.sendKeys("Rodrigues");
		
		// Passo 5
		btnCadastrar.click();
		// Passo 6
		Alert alertSexo = driver.switchTo().alert();
		String mensagemAlertSexo = alertSexo.getText();
		Assert.assertEquals("Sexo eh obrigatorio", mensagemAlertSexo);
		alertSexo.accept();
//		driver.switchTo().window(" ");
		WebElement inputRadioSexo = driver.findElement(By.id("elementosForm:sexo:0"));
		inputRadioSexo.click();
		
		// Passo 7.1
		WebElement comida1 = driver.findElement(By.id("elementosForm:comidaFavorita:0"));
		WebElement comida2 = driver.findElement(By.id("elementosForm:comidaFavorita:1"));
		WebElement comida3 = driver.findElement(By.id("elementosForm:comidaFavorita:2"));
		comida1.click();
		comida2.click();
		comida3.click();
		
		// Passo 7.2
		WebElement escolaridade = driver.findElement(By.id("elementosForm:escolaridade"));
		Select selectEscolaridade = new Select(escolaridade);
		selectEscolaridade.selectByValue("superior");
		Assert.assertEquals("Superior", selectEscolaridade.getFirstSelectedOption().getText());
		
		// Passo 7.3
		WebElement esporte = driver.findElement(By.id("elementosForm:esportes"));
		Select selectEsportes = new Select(esporte);
		List<WebElement> listaEsportes = selectEsportes.getOptions();
		listaEsportes.forEach(l -> {
			if(l.getText().equals("Futebol"))
				l.click();
		});
		
		// Passo 7.4
		WebElement sugestoes = driver.findElement(By.id("elementosForm:sugestoes"));
		sugestoes.sendKeys("Nada a comentar!!!");
		
		// Passo 8
		btnCadastrar.click();
		WebElement textView01 = driver.findElement(By.xpath("//*[@id=\"resultado\"]/span"));
		String mensagemTexto01 = textView01.getText();
		Assert.assertEquals("Cadastrado!", mensagemTexto01);
		
		WebElement textViewNome = driver.findElement(By.xpath("//*[@id=\"descNome\"]/span"));
		String mensagemTextNome = textViewNome.getText();
		Assert.assertEquals("Thiago", mensagemTextNome);
		
		WebElement textViewSobrenome = driver.findElement(By.xpath("//*[@id=\"descSobrenome\"]/span"));
		String  mensagemTextSobrenome = textViewSobrenome.getText();
		Assert.assertEquals("Rodrigues", mensagemTextSobrenome);
		
		WebElement textViewSexo = driver.findElement(By.xpath("//*[@id=\"descSexo\"]/span"));
		String mensagemTextSexo = textViewSexo.getText();
		Assert.assertEquals("Masculino", mensagemTextSexo);
		
		WebElement textViewComida = driver.findElement(By.cssSelector("#descComida > span"));
		String mensagemTextComida = textViewComida.getText();
		Assert.assertEquals("Carne Frango Pizza", mensagemTextComida);
		
		WebElement textViewEscolaridade = driver.findElement(By.cssSelector("#descEscolaridade > span"));
		String mensagemTextEscolaridade = textViewEscolaridade.getText();
		Assert.assertEquals("superior", mensagemTextEscolaridade);
		
		WebElement textViewEsportes = driver.findElement(By.cssSelector("#descEsportes > span"));
		String mensagemTextEsportes = textViewEsportes.getText();
		Assert.assertEquals("Futebol", mensagemTextEsportes);
		
		WebElement textViewSugestoes = driver.findElement(By.cssSelector("#descSugestoes > span"));
		String mensagemTextSugestoes = textViewSugestoes.getText();
		Assert.assertEquals("Nada a comentar!!!", mensagemTextSugestoes);
		
		Thread.sleep(5000);
		driver.quit();
	}
}
